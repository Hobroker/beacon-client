const express = require('express');
const app = express();
const http = require('http').Server(app);
const path = require('path');

const port = 3001;

app.get('/', function (req, res) {
	res.sendFile(path.join(__dirname + '/public/index.html'));
});

http.listen(port, function () {
	console.log('Listening on port %d', port)
});

app.use(express.static('public'));