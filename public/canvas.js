var box = document.getElementById("box");
var ctx = box.getContext("2d");

var ROOM_SIZE = 200;
var HOUSE_SPACE = 20;
var WALL_WEIGHT = 2;
var EMITOR_SIZE = 10;
var CAM_SIZE = 40;

var FIRST_TIME = true;

var USER_FOLLOWED_COLOR = 'rgba(0, 0, 255, .5)';
var USER_UNFOLLOWED_COLOR = 'rgba(0, 255, 0, .5)';
var EMITOR_COLOR = 'rgba(255, 0, 0, .5)';

var CAM_ICON = '/media/cam_v2.svg';

function skeleton() {
	for (var i = 0; i < 4; i++) {
		help.rect(HOUSE_SPACE + ROOM_SIZE * i, HOUSE_SPACE);
	}
	help.rect(HOUSE_SPACE, HOUSE_SPACE + ROOM_SIZE);
	help.rect(HOUSE_SPACE + ROOM_SIZE * 3, HOUSE_SPACE + ROOM_SIZE);
}

var help = {
	rect: function rect(x, y) {
		ctx.lineWidth = WALL_WEIGHT;
		ctx.strokeRect(x, y, ROOM_SIZE, ROOM_SIZE);
	},
	rand: function rand(min, max) {
		return Math.floor(Math.random() * (max - min + 1) + min);
	},
	circle: function (data) {
		data.x += HOUSE_SPACE;
		data.y += HOUSE_SPACE;
		ctx.fillStyle = data.color;
		ctx.beginPath();
		ctx.arc(data.x, data.y, EMITOR_SIZE, 0, 2 * Math.PI);
		ctx.fill();

		if (data.text === undefined)
			return;
		ctx.font = '10pt Arial';
		ctx.textAlign = 'center';
		ctx.fillStyle = 'red';
		ctx.fillText(data.text, data.x, data.y + 4);
	},
	clone: function (from, to) {
		for (var key in from)
			if (from.hasOwnProperty(key))
				to[key] = from[key];
	},
	putCamIcon: function ({x, y}) {
		x += HOUSE_SPACE;
		y += HOUSE_SPACE;
		var img = new Image();
		img.onload = function () {
			ctx.drawImage(img, x - CAM_SIZE / 2, y - CAM_SIZE / 2, CAM_SIZE, CAM_SIZE);
		};
		img.src = CAM_ICON;
	},
	updateIframe: function (link) {
		var frame = document.getElementById('currentCam');
		if (frame.src == link)
			return;
		frame.src = link;

		if(FIRST_TIME) {
			FIRST_TIME = false;
			plyr.setup();
		}
	}
};
