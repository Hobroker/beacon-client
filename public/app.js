var SERVER = 'https://best-hackathon-server.herokuapp.com';
// var SERVER = 'http://localhost:3000';

var SOCKET_UPDATE_USERS = 'users';
var SOCKET_NEW_MESSAGE = 'new message';

var URL_USERS_LIST = SERVER + '/user/list';
var URL_EMITORS_LIST = SERVER + '/emitor/list';
var URL_CAMERA_LIST = SERVER + '/camera/list';

var app = angular.module('beacon', ['ngMaterial']);
var socket = io.connect('ws://best-hackathon-server.herokuapp.com:80');
// var socket = io.connect('ws://localhost:3000');

app.controller('DeskCtrl', function ($scope, $http, $timeout, Emitor, User, Camera) {
	var FOLLOWED_ID;

	box.width = ROOM_SIZE * 4 + HOUSE_SPACE * 2;
	box.height = ROOM_SIZE * 2 + HOUSE_SPACE * 2;

	$scope.emitors = [];
	$scope.users = [];
	$scope.cameras = [];

	var Canvas = {
		refresh: function () {
			ctx.clearRect(0, 0, box.width, box.height);
			skeleton();
			this.draw();
		},
		draw: function () {
			$.each($scope.cameras, function (id, camera) {
				help.putCamIcon(camera.position);
			});

			$.each($scope.emitors, function (id, emitor) {
				help.circle({
					x: emitor.position.x,
					y: emitor.position.y,
					color: EMITOR_COLOR
				});
			});

			$.each($scope.users, function (id, user) {
				help.circle({
					x: user.position.x,
					y: user.position.y,
					color: user.followed ? USER_FOLLOWED_COLOR : USER_UNFOLLOWED_COLOR,
					text: user.id
				});
			});
		}
	};

	var getUserByID = function (id) {
		return $scope.users.filter(function (user) {
			return user.id == id
		})[0]
	};

	$http.get(URL_EMITORS_LIST).success(function (response) {
		$scope.emitors = [];
		response.forEach(function (item) {
			$scope.emitors.push(new Emitor(item));
		});
		Canvas.refresh();
	}).error(function (error) {
		console.log(error);
	});

	$http.get(URL_USERS_LIST).success(function (response) {
		$scope.users = [];
		response.forEach(function (item) {
			$scope.users.push(new User(item));
		});
		Canvas.refresh();
	}).error(function (error) {
		console.log(error);
	});

	$http.get(URL_CAMERA_LIST).success(function (response) {
		$scope.cameras = [];
		response.forEach(function (item) {
			$scope.cameras.push(new Camera(item));
		});
		Canvas.refresh();
	}).error(function (error) {
		console.log(error);
	});

	$scope.follow = function (user) {
		FOLLOWED_ID = user.id;
		$scope.users.forEach(function (user) {
			user.followed = false;
		});
		user.followed = true;
		help.updateIframe(user.stream);
		Canvas.refresh();
	};

	socket.on(SOCKET_UPDATE_USERS, function (response) {
		$timeout(function () {
			var data = response.message;
			var IDs = [];
			console.log('/publish (%d users)', data.length);
			data.forEach(function (item) {
				var user = getUserByID(item.id);
				if (user === undefined) {
					$scope.users.push(new User(item));
					return;
				}
				IDs.push(item.id);
				user.stream = item.stream;
				if (FOLLOWED_ID == user.id) {
					help.updateIframe(user.stream)
				}
				user.position = item.position
			});
			$scope.users.filter(function (user) {
				return IDs.indexOf(user.id) == -1;
			});
			Canvas.refresh();
		});
	});

	Canvas.refresh();
});